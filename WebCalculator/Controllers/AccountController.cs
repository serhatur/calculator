﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using WebCalculator.Models;

namespace WebCalculator.Controllers
{

    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User model)
        {
            //get authorization key to secure login 
            FormsAuthentication.SetAuthCookie(model.Username, false);

            string username;
            string pwd;
            string CurrentUser = "";
            string CurrentPwd = "";
            bool LoginStatus = false;

            //check model is right , if not it returns error message
            if (ModelState.IsValid)
            {
                username = model.Username;
                pwd = model.Password;

                //Used xml file to store usr and pwd information 
                XmlDocument xmxdoc = new XmlDocument();
                xmxdoc.Load(Server.MapPath("~/App_Data/User.xml"));
                XmlNodeList xmlnodelist = xmxdoc.GetElementsByTagName("User");
                foreach (XmlNode xn in xmlnodelist)
                {
                    XmlNodeList xmlnl = xn.ChildNodes;
                    foreach (XmlNode xmln in xmlnl)
                    {
                        if (xmln.Name == "Username")
                        {
                            if (xmln.InnerText == username)
                            {
                                CurrentUser = username;
                            }
                        }
                        if (xmln.Name == "Password")
                        {
                            if (xmln.InnerText == pwd)
                            {
                                CurrentPwd = pwd;
                            }
                        }
                    }
                    if ((CurrentUser != "") & (CurrentPwd != ""))
                    {
                        LoginStatus = true;
                        return Redirect("/Calculate/Index");
                    }
                }
            }
            else
            {
                LoginStatus = true;
            }
            ViewBag.LoginStatus = LoginStatus;
            return View(model);
        }

        public ActionResult LogOut()
        {
            //remove authorization key to secure sign out 
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Calculate");
        }
    }
}