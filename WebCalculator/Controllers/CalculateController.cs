﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebCalculator.Controllers
{
    //Used authorize to avoid to enter calculate pages before login
    [Authorize]
    public class CalculateController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ReturnResult(string data)
        {
            DataTable dt = new DataTable();
            //Used compute function to calculate
            var answer = dt.Compute(data, "");

            return Json(answer, JsonRequestBehavior.AllowGet);
        }
    }
}